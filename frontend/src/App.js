import { useMemo } from 'react';
import data from './res-second-task.json';
import {
  MaterialReactTable,
  useMaterialReactTable,
} from 'material-react-table';
import { MRT_Localization_RU } from 'material-react-table/locales/ru';

function App() {
  const realData = useMemo(() => {
    const preparedArray = [];

    const iterate = (obj, id) => {
      for (const key in obj) {
        if (typeof obj[key] === 'object' && obj[key] !== null) {
          const isRealData = !['children', 'data'].includes(key);
          const uniqueKey = `${id ? `${id}#` : ''}${key}`;
          if (isRealData) {
            const splittedKeys = uniqueKey.split('#');
            const dataToPush = {
              id: uniqueKey,
              name: key,
              data: obj[key]?.data,
              children: [],
            };
            if (splittedKeys.length === 1) {
              preparedArray.push(dataToPush);
            } else {
              let arrayToSearch = preparedArray;
              splittedKeys.forEach((k, idx) => {
                const parent = arrayToSearch.find((p) => p.name === k);
                if (!parent) return;
                if (idx === splittedKeys.length - 2) {
                  parent.children.push(dataToPush);
                  return;
                }
                arrayToSearch = parent.children;
              });
            }
          }
          iterate(obj[key], isRealData ? uniqueKey : id);
        }
      }
    };

    iterate(data);
    return preparedArray;
  }, []);
  const columns = useMemo(() => {
    return [
      {
        header: 'Наименование',
        accessorFn: (row) => {
          return row.name;
        },
      },
      {
        header: 'Год 2020',
        accessorFn: (row) => {
          return row?.data?.[2020] || '';
        },
      },
      {
        header: 'Год 2021',
        accessorFn: (row) => {
          return row?.data?.[2021] || '';
        },
      },
      {
        header: 'Год 2022',
        accessorFn: (row) => {
          return row?.data?.[2022] || '';
        },
      },
      {
        header: 'Год 2023',
        accessorFn: (row) => {
          return row?.data?.[2023] || '';
        },
      },
      {
        header: 'Год 2024',
        accessorFn: (row) => {
          return row?.data?.[2024] || '';
        },
      },
    ];
  }, []);

  const table = useMaterialReactTable({
    enableExpandAll: false,
    filterFromLeafRows: true,
    enableStickyHeader: true,
    localization: MRT_Localization_RU,
    muiTableContainerProps: { sx: { maxHeight: '600px' } },
    layoutMode: 'grid',
    paginateExpandedRows: false,
    enableRowPinning: true,
    enableExpanding: true,
    getSubRows: (row) => row.children,
    enableColumnFilters: false,
    columns,
    data: realData,
  });
  return <MaterialReactTable table={table} />;
}

export default App;
