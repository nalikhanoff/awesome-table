const express = require('express');
const { readFile } = require('fs/promises');
const { Client } = require('pg');

const app = express();

// please use dotenv or some other environment variable libraries to keep connection string in real projects
// this project only for demonstration.
// please launch postgres server in docker by entering command: docker compose up
const connection = {
  user: 'postgres',
  host: 'localhost',
  password: 'postgres',
  port: 5432,
  database: 'postgres',
};

app.get('/', async (_, res) => {
  const databaseClient = new Client(connection);
  await databaseClient.connect();

  const queryString = `
  SELECT 
    project, constructive, under_constructive, work_group, block, floor, level,
    min(base_plan_start_date) AS min_base_plan_start_date, 
    max(base_plan_finish_date) AS max_base_plan_finish_date,
    (SELECT 
      json_agg(
        json_build_object(
          'contractor', contractor,
          'plan_value', plan_value
        )
        order by contractor IS NULL ASC, plan_value DESC
      ) AS contractors
    FROM
      table_first_task t2
    WHERE
      t2.project = table_first_task.project AND
      t2.constructive = table_first_task.constructive AND
      t2.under_constructive = table_first_task.under_constructive AND
      t2.work_group = table_first_task.work_group AND
      t2.block = table_first_task.block AND
      t2.floor = table_first_task.floor AND 
      t2.level = table_first_task.level
    )
  FROM table_first_task GROUP BY project, constructive, under_constructive, work_group, block, floor, level;
`;

  const { rows } = await databaseClient.query(queryString);
  res.json(rows);
});

app.listen(3000, async () => {
  console.log('server started on port:', 3000);
  const databaseClient = new Client(connection);
  await databaseClient.connect();
  const { rows: resultOfTest } = await databaseClient.query(
    'SELECT 1 + 1 as RESULT'
  );
  console.log('test connection: ', resultOfTest);

  await databaseClient.query('DROP TABLE IF EXISTS table_first_task CASCADE;');
  const sqlFileContent = await readFile('./table_first_task.sql', 'utf8');
  await databaseClient.query(sqlFileContent);
  const { rows: insertedRowsCount } = await databaseClient.query(
    'SELECT count(*) FROM table_first_task;'
  );
  console.log('inserted rows count:', insertedRowsCount);
  await databaseClient.end();
});
